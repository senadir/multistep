var gulp         = require('gulp');
var browserSync  = require('browser-sync').create();
var sass         = require('gulp-sass');
var htmlInjector = require("bs-html-injector");
var ts           = require('gulp-typescript');
var htmlsplit    = require('gulp-htmlsplit');
var cssmin       = require('gulp-cssmin');
var imagemin     = require('gulp-imagemin');
var uglify       = require('gulp-uglify');
var pump         = require('pump');
var sourcemaps   = require('gulp-sourcemaps');

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'typescript'], function() {

    browserSync.init({
        server: "./"
    });
    browserSync.use(htmlInjector, {
        files: "*.html"
    });
    gulp.watch("typescript/*.ts", ['typescript'])
    gulp.watch("scss/*.scss", ['sass']);
    gulp.watch("*.html").on('change', htmlInjector);

});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write("/"))
        .pipe(gulp.dest("css"))
        .pipe(browserSync.stream());
});

gulp.task('typescript', function () {
    return (gulp.src('typescript/main.ts')
        .pipe(sourcemaps.init())
        .pipe(ts({
            noImplicitAny: false,
            out: 'app.js'
        }))
        .pipe(sourcemaps.write("/"))
        .pipe(gulp.dest('js'))
        .pipe(browserSync.stream()),
        gulp.src('typescript/storage.ts')
            .pipe(ts({
                noImplicitAny: false,
                out: 'storage.js'
            }))
            .pipe(gulp.dest('js'))
            .pipe(browserSync.stream()))
});

gulp.task('imagemin', function() {
  return gulp.src('assets/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/assets'))
})
gulp.task('htmlsplit', function() {
  gulp.src('./*.html')
    .pipe(htmlsplit())
    .pipe(gulp.dest('build'));
})

gulp.task('cssmin', function() {
    return gulp.src('css/master.css')
        .pipe(cssmin())
        .pipe(gulp.dest('./build/css'));
});

gulp.task('uglify', function (cb) {
  pump([
        gulp.src('js/*.js'),
        uglify(),
        gulp.dest('build/js')
    ],
    cb
  );
});
gulp.task('default', ['serve']);
gulp.task('full-build', ['sass', 'typescript', 'imagemin', 'htmlsplit', 'cssmin', 'uglify']);

gulp.task('build', ['sass', 'typescript', 'htmlsplit', 'cssmin', 'uglify']);
