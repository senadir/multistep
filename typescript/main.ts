"use strict";

class Progress {

    currentStep: number = 0;

    currrentElement: Element;

    nextElement: Element;

    breadcrumbsList: NodeListOf < Element > ;

    optionList: NodeListOf < Element > ;

    stepsList: NodeListOf < Element > = document.querySelectorAll(".form-step");

    steps: Array < string > = ["turnaround", "paper", "sides", "orientation", "corners"];

    // existing links have another order that dosen't fellow this app order, changing the order
    // of the app to match the legacy will break the app and the logic (since step 4 can't be before 3)
    // for obvious reasons since 4 view depends on choices made in 3
    // and changing the all the legacy link is not possible, so this list is used to create the match
    legacyStepsOrder: Array < string > = ["turnaround", "paper", "orientation", "sides", "corners"];

    // those values can hide or void steps after them, so we watch for them
    caseChangingSteps = {
        "turnaround": ["5-14"],
        "paper": ["uncoated"]
    };

    self: Progress = this;

    dataBinding = {
        "3-5": "1",
        "5-14": "2",
        "u-v-coated": "uvcoated",
        "matte-glossy": "matte",
        "uncoated": "uncoated",
        "horizontal": "1",
        "vertical": "2",
        "1-sided": "1",
        "2-sided": "2",
        "standard": "1",
        "14-round": "14",
        "18-round": "18"
    };

    choices: Object = {};

    constructor() {
        this.getPreviousChoices();
        this.setInitialState();
        this.updateBreadcrumbs();
        this.eventDispatcher();
    }

    getValueFromKey(key: string): string {
        if (this.dataBinding.hasOwnProperty(key)) {
            return this.dataBinding[key];
        } else {
            return key;
        };
    };

    setInitialState() {
        // convert a NodeList to Array of references so we can forEach it
        let stepsNodes = Array.prototype.slice.call(this.stepsList, 0);

        // hide all steps, no exceptions
        stepsNodes.forEach(function(step) {
            step.classList.remove("form-step--current");
            let options = step.querySelectorAll(".option-container input");
            options = Array.prototype.slice.call(options, 0);
            this.steps.forEach(function(step) {
             options.forEach( function(option) {
              if (this.choices[step]) {
               if (option.id == this.choices[step]) {
                option.checked = true;
               }
              }

             }.bind(this));
            }.bind(this));

        }.bind(this));

        document.querySelector(".step__" + this.steps[this.currentStep]).classList.add("form-step--current");
    };

    stepHasValue(stepNumber: number) {
        let optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        let hasValue = false;
        let options = Array.prototype.slice.call(optionNodes, 0);

        options.forEach(function(option) {
            if (option.querySelector("input").checked) {
                hasValue = true;
            }
        });
        return hasValue;
    }

    updateBreadcrumbs() {
        this.breadcrumbsList = document.querySelector(".cd-breadcrumb").getElementsByTagName("li");

        for (let i = 0; i < this.breadcrumbsList.length; i++) {
            // make sure no step is still set as the current
            this.breadcrumbsList[i].classList.remove("current");
            // all steps that are before the current step are considired visited
            if (i < this.currentStep) {
                this.breadcrumbsList[i].classList.add("visited");
            } else if (i === this.currentStep) {
                // and our current step should have current
                this.breadcrumbsList[i].classList.remove("visited");
                this.breadcrumbsList[i].classList.add("current");
            } else {
                // future steps have nothing
                this.breadcrumbsList[i].classList.remove("visited");
            }
        }
    }
    showCurrentStep(stepNumber: number = this.currentStep) {
        this.currrentElement = document.querySelector(".step__" + this.steps[stepNumber]);
        let steps = Array.prototype.slice.call(this.stepsList, 0);
        this.updateBreadcrumbs();
        steps.forEach(function(step) {
            if (this.currrentElement !== step) {
                // hide steps that are not the current step
                this.hideStep(step);
            } else {
                this.showStep(this.currrentElement);
            }
        }.bind(this));

    }

    hideStep(step: HTMLElement) {
        step.style.opacity = "0";
        setTimeout(function() {
            step.style.display = "none";
        }.bind(step), 200);

    }

    showStep(step: HTMLElement) {
        step.style.display = "block";
        step.style.position = "absolute";
        setTimeout(function() {
            step.style.opacity = "1";
            step.style.position = "relative";
        }.bind(step), 200);
    }
    stepVoidRest(stepName: string, option: string) {
        if (this.caseChangingSteps[stepName] && this.caseChangingSteps[stepName].indexOf(option) !== -1) {
            return true;
        } else {
            return false;
        }
    }
    registerChoice(stepNumber: number = this.currentStep) {
        let optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        let choice = undefined;
        let options = Array.prototype.slice.call(optionNodes, 0);
        options.forEach(function(option) {
            if (option.querySelector("input").checked) {
                choice = option.querySelector("input").id;
            }
        });

        this.saveValue(stepNumber, choice);
        if (this.stepVoidRest(this.steps[stepNumber], choice)) {
            for (let i = this.currentStep + 1; i < this.steps.length; i++) {
                this.voidChoice(i);
            }
        }
    }

    voidChoice(stepNumber: number = this.currentStep) {
        let optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        let options = Array.prototype.slice.call(optionNodes, 0);

        options.forEach(function(option) {
            if (option.querySelector("input").checked) {
                option.querySelector("input").checked = false;
            }
        });

        this.saveValue(stepNumber, undefined);
    }

    updateNextStepsConditions() {
        let conditionClass = "has-" + this.steps[this.currentStep - 1] + "-" + this.choices[this.steps[this.currentStep - 1]];
        for (let i = this.currentStep; i < this.steps.length; i++) {
            [].slice.call(this.stepsList[i].classList).forEach(className => {
                let conditionClassStep = "has-" + this.steps[this.currentStep - 1];
                if (className.substring(0, conditionClassStep.length) === conditionClassStep) {
                    this.stepsList[i].classList.remove(className);
                };
            });
            this.stepsList[i].classList.add(conditionClass);
        }
    }
    checkForEmptySteps() {
        let EmptyStepsExist = false;
        for (let i = 0; i < this.steps.length; i++) {
            if (!this.stepHasValue(i)) {
                EmptyStepsExist = true;
                break;
            }
        }
        return EmptyStepsExist;
    }

    gotoFirstEmptyStep() {
        for (let i = 0; i < this.steps.length; i++) {
            if (!this.stepHasValue(i)) {
                this.currentStep = i;
                this.showCurrentStep(i);
                break;
            }
        }
    }

    generateLink() {
        let link = "http://weshipcards.com/product/";
        let optionsList = [];
        let self = this;
        this.legacyStepsOrder.forEach(step => {
            optionsList.push(self.getValueFromKey(self.choices[step]));
        });
        link += optionsList.join("-");
        localStorage.removeItem("multistepChoices");
        localStorage.removeItem("step");
        window.location.href = link;
    }
    getPreviousChoices() {
     if ( localStorage.getItem("multistepChoices") && localStorage.getItem("step") ) {
      this.choices = JSON.parse(localStorage.getItem("multistepChoices"));
      this.currentStep = parseInt(localStorage.getItem("step"));
     }
    }
    saveValue(stepNumber: number, stepValue: string) {
     this.choices[this.steps[stepNumber]] = stepValue;
     localStorage.setItem("multistepChoices", JSON.stringify(this.choices));
     localStorage.setItem("step", (this.currentStep + 1).toString());
    }
    eventDispatcher() {
        this.optionList = document.getElementsByClassName("option-container");

        for (let i = 0; i < this.optionList.length; i++) {
            let self = this;
            this.optionList[i].addEventListener("click", function(e) {
                // the actions where fired twice, this will cause rendering problems
                // so we emit the first action fire.
                e.stopPropagation();
                e.preventDefault();
                // since we prevent the default checkbox behavior we need to check the input manually
                (this.getElementsByTagName("input"))[0].checked = true;
                self.registerChoice();
                self.currentStep++;
                self.updateNextStepsConditions();
                if (self.currentStep >= self.steps.length) {
                    if (self.checkForEmptySteps()) {
                        self.gotoFirstEmptyStep();
                    } else {
                        self.generateLink();
                    }
                } else {
                    self.showCurrentStep();
                }

            }, true);
        }

        for (let i = 0; i < this.breadcrumbsList.length; i++) {
            this.breadcrumbsList[i].addEventListener("click", function(e) {
                e.stopPropagation();
                e.preventDefault();
                this.currentStep = i;
                this.showCurrentStep();
            }.bind(this), true);
        }
    }
}

let multistep = new Progress();
