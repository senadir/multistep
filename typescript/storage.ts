class MultistepAppClass {
 productLink: string =  localStorage.getItem("productLink");
 getProductLink() {
  if (!this.productLink) {
   return null;
  } else {
   return this.productLink;
  }
 }

 redirectToProduct() {
  window.location.href = this.getProductLink()
 }
}

let MultistepApp = new MultistepAppClass();
