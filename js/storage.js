var MultistepAppClass = (function () {
    function MultistepAppClass() {
        this.productLink = localStorage.getItem("productLink");
    }
    MultistepAppClass.prototype.getProductLink = function () {
        if (!this.productLink) {
            return null;
        }
        else {
            return this.productLink;
        }
    };
    MultistepAppClass.prototype.redirectToProduct = function () {
        window.location.href = this.getProductLink();
    };
    return MultistepAppClass;
}());
var MultistepApp = new MultistepAppClass();
