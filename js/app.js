"use strict";
var Progress = (function () {
    function Progress() {
        this.currentStep = 0;
        this.stepsList = document.querySelectorAll(".form-step");
        this.steps = ["turnaround", "paper", "sides", "orientation", "corners"];
        // existing links have another order that dosen't fellow this app order, changing the order
        // of the app to match the legacy will break the app and the logic (since step 4 can't be before 3)
        // for obvious reasons since 4 view depends on choices made in 3
        // and changing the all the legacy link is not possible, so this list is used to create the match
        this.legacyStepsOrder = ["turnaround", "paper", "orientation", "sides", "corners"];
        // those values can hide or void steps after them, so we watch for them
        this.caseChangingSteps = {
            "turnaround": ["5-14"],
            "paper": ["uncoated"]
        };
        this.self = this;
        this.dataBinding = {
            "3-5": "1",
            "5-14": "2",
            "u-v-coated": "uvcoated",
            "matte-glossy": "matte",
            "uncoated": "uncoated",
            "horizontal": "1",
            "vertical": "2",
            "1-sided": "1",
            "2-sided": "2",
            "standard": "1",
            "14-round": "14",
            "18-round": "18"
        };
        this.choices = {};
        this.getPreviousChoices();
        this.setInitialState();
        this.updateBreadcrumbs();
        this.eventDispatcher();
    }
    Progress.prototype.getValueFromKey = function (key) {
        if (this.dataBinding.hasOwnProperty(key)) {
            return this.dataBinding[key];
        }
        else {
            return key;
        }
        ;
    };
    ;
    Progress.prototype.setInitialState = function () {
        // convert a NodeList to Array of references so we can forEach it
        var stepsNodes = Array.prototype.slice.call(this.stepsList, 0);
        // hide all steps, no exceptions
        stepsNodes.forEach(function (step) {
            step.classList.remove("form-step--current");
            var options = step.querySelectorAll(".option-container input");
            options = Array.prototype.slice.call(options, 0);
            this.steps.forEach(function (step) {
                options.forEach(function (option) {
                    if (this.choices[step]) {
                        if (option.id == this.choices[step]) {
                            option.checked = true;
                        }
                    }
                }.bind(this));
            }.bind(this));
        }.bind(this));
        document.querySelector(".step__" + this.steps[this.currentStep]).classList.add("form-step--current");
    };
    ;
    Progress.prototype.stepHasValue = function (stepNumber) {
        var optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        var hasValue = false;
        var options = Array.prototype.slice.call(optionNodes, 0);
        options.forEach(function (option) {
            if (option.querySelector("input").checked) {
                hasValue = true;
            }
        });
        return hasValue;
    };
    Progress.prototype.updateBreadcrumbs = function () {
        this.breadcrumbsList = document.querySelector(".cd-breadcrumb").getElementsByTagName("li");
        for (var i = 0; i < this.breadcrumbsList.length; i++) {
            // make sure no step is still set as the current
            this.breadcrumbsList[i].classList.remove("current");
            // all steps that are before the current step are considired visited
            if (i < this.currentStep) {
                this.breadcrumbsList[i].classList.add("visited");
            }
            else if (i === this.currentStep) {
                // and our current step should have current
                this.breadcrumbsList[i].classList.remove("visited");
                this.breadcrumbsList[i].classList.add("current");
            }
            else {
                // future steps have nothing
                this.breadcrumbsList[i].classList.remove("visited");
            }
        }
    };
    Progress.prototype.showCurrentStep = function (stepNumber) {
        if (stepNumber === void 0) { stepNumber = this.currentStep; }
        this.currrentElement = document.querySelector(".step__" + this.steps[stepNumber]);
        var steps = Array.prototype.slice.call(this.stepsList, 0);
        this.updateBreadcrumbs();
        steps.forEach(function (step) {
            if (this.currrentElement !== step) {
                // hide steps that are not the current step
                this.hideStep(step);
            }
            else {
                this.showStep(this.currrentElement);
            }
        }.bind(this));
    };
    Progress.prototype.hideStep = function (step) {
        step.style.opacity = "0";
        setTimeout(function () {
            step.style.display = "none";
        }.bind(step), 200);
    };
    Progress.prototype.showStep = function (step) {
        step.style.display = "block";
        step.style.position = "absolute";
        setTimeout(function () {
            step.style.opacity = "1";
            step.style.position = "relative";
        }.bind(step), 200);
    };
    Progress.prototype.stepVoidRest = function (stepName, option) {
        if (this.caseChangingSteps[stepName] && this.caseChangingSteps[stepName].indexOf(option) !== -1) {
            return true;
        }
        else {
            return false;
        }
    };
    Progress.prototype.registerChoice = function (stepNumber) {
        if (stepNumber === void 0) { stepNumber = this.currentStep; }
        var optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        var choice = undefined;
        var options = Array.prototype.slice.call(optionNodes, 0);
        options.forEach(function (option) {
            if (option.querySelector("input").checked) {
                choice = option.querySelector("input").id;
            }
        });
        this.saveValue(stepNumber, choice);
        if (this.stepVoidRest(this.steps[stepNumber], choice)) {
            for (var i = this.currentStep + 1; i < this.steps.length; i++) {
                this.voidChoice(i);
            }
        }
    };
    Progress.prototype.voidChoice = function (stepNumber) {
        if (stepNumber === void 0) { stepNumber = this.currentStep; }
        var optionNodes = document.querySelectorAll(".step__" + this.steps[stepNumber] + " .option-container");
        var options = Array.prototype.slice.call(optionNodes, 0);
        options.forEach(function (option) {
            if (option.querySelector("input").checked) {
                option.querySelector("input").checked = false;
            }
        });
        this.saveValue(stepNumber, undefined);
    };
    Progress.prototype.updateNextStepsConditions = function () {
        var _this = this;
        var conditionClass = "has-" + this.steps[this.currentStep - 1] + "-" + this.choices[this.steps[this.currentStep - 1]];
        var _loop_1 = function(i) {
            [].slice.call(this_1.stepsList[i].classList).forEach(function (className) {
                var conditionClassStep = "has-" + _this.steps[_this.currentStep - 1];
                if (className.substring(0, conditionClassStep.length) === conditionClassStep) {
                    _this.stepsList[i].classList.remove(className);
                }
                ;
            });
            this_1.stepsList[i].classList.add(conditionClass);
        };
        var this_1 = this;
        for (var i = this.currentStep; i < this.steps.length; i++) {
            _loop_1(i);
        }
    };
    Progress.prototype.checkForEmptySteps = function () {
        var EmptyStepsExist = false;
        for (var i = 0; i < this.steps.length; i++) {
            if (!this.stepHasValue(i)) {
                EmptyStepsExist = true;
                break;
            }
        }
        return EmptyStepsExist;
    };
    Progress.prototype.gotoFirstEmptyStep = function () {
        for (var i = 0; i < this.steps.length; i++) {
            if (!this.stepHasValue(i)) {
                this.currentStep = i;
                this.showCurrentStep(i);
                break;
            }
        }
    };
    Progress.prototype.generateLink = function () {
        var link = "http://weshipcards.com/product/";
        var optionsList = [];
        var self = this;
        this.legacyStepsOrder.forEach(function (step) {
            optionsList.push(self.getValueFromKey(self.choices[step]));
        });
        link += optionsList.join("-");
        localStorage.removeItem("multistepChoices");
        localStorage.removeItem("step");
        window.location.href = link;
    };
    Progress.prototype.getPreviousChoices = function () {
        if (localStorage.getItem("multistepChoices") && localStorage.getItem("step")) {
            this.choices = JSON.parse(localStorage.getItem("multistepChoices"));
            this.currentStep = parseInt(localStorage.getItem("step"));
        }
    };
    Progress.prototype.saveValue = function (stepNumber, stepValue) {
        this.choices[this.steps[stepNumber]] = stepValue;
        localStorage.setItem("multistepChoices", JSON.stringify(this.choices));
        localStorage.setItem("step", (this.currentStep + 1).toString());
    };
    Progress.prototype.eventDispatcher = function () {
        this.optionList = document.getElementsByClassName("option-container");
        var _loop_2 = function(i) {
            var self_1 = this_2;
            this_2.optionList[i].addEventListener("click", function (e) {
                // the actions where fired twice, this will cause rendering problems
                // so we emit the first action fire.
                e.stopPropagation();
                e.preventDefault();
                // since we prevent the default checkbox behavior we need to check the input manually
                (this.getElementsByTagName("input"))[0].checked = true;
                self_1.registerChoice();
                self_1.currentStep++;
                self_1.updateNextStepsConditions();
                if (self_1.currentStep >= self_1.steps.length) {
                    if (self_1.checkForEmptySteps()) {
                        self_1.gotoFirstEmptyStep();
                    }
                    else {
                        self_1.generateLink();
                    }
                }
                else {
                    self_1.showCurrentStep();
                }
            }, true);
        };
        var this_2 = this;
        for (var i = 0; i < this.optionList.length; i++) {
            _loop_2(i);
        }
        var _loop_3 = function(i) {
            this_3.breadcrumbsList[i].addEventListener("click", function (e) {
                e.stopPropagation();
                e.preventDefault();
                this.currentStep = i;
                this.showCurrentStep();
            }.bind(this_3), true);
        };
        var this_3 = this;
        for (var i = 0; i < this.breadcrumbsList.length; i++) {
            _loop_3(i);
        }
    };
    return Progress;
}());
var multistep = new Progress();

//# sourceMappingURL=app.js.map
