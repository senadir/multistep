start working on the project on your own machine
first pull the repo

```
git clone https://senadir@bitbucket.org/senadir/multistep.git
cd multistep
npm install
```


develop

```
gulp
```

build a production version

```
gulp full-build
```

upload the `build` folder to the theme, and all is fine
